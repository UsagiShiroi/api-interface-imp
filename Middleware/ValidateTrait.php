<?php

namespace Middleware;

/**
 * Class ValidateTrait
 * @trait ValidateTrait
 * @package Middleware
 */
trait ValidateTrait
{
    /**
     * @var
     * Правила валидации, массив
     */
    protected $rules;
    /**
     * @var
     * Значение,которое валидируется
     */
    protected $field_value;
    /**
     * @var
     * Значение, определяющее валидацию
     */
    protected $rule_value;

    /**
     * @param array $fields - массив полей для валидации {имя правила} => {проверяемое значение}
     * @return array
     *
     * Производит валидацию значений переданных полей
     */
    protected function validate(Array $fields)
    {
        $validation_errors = [];
        foreach ($fields as $field_name => $field_value) {
            if (!array_key_exists($field_name, $this->rules)) {
                continue;
            }
            foreach ($this->rules[$field_name] as $method => $value) {
                if (method_exists($this, $method)) {
                    $this->field_value = $field_value;
                    $this->rule_value = $value;
                    if ($this->{$method}() === false) {
                        $validation_errors[] = $field_name;
                    }
                }
            }
        }
        return $validation_errors;
    }

    /**
     * @param $fields - массив обязательных полей, [{название поля 1}, {название поля 2}]
     * @return array
     *
     * Проверяет переданы-ли все необходимые параметры
     */
    protected function required($fields)
    {
        $missing_fields = [];
        foreach ($fields as $field_name) {
            if (!isset($_REQUEST[$field_name])) {
                $missing_fields[] = $field_name;
            }
        }
        return $missing_fields;
    }

    /**
     * @param string $name - название поля, которое необходимо поулчить.
     * @param null|mixed $default_value - значение, возвращаеиое, если поле не было передано в запросе.
     * @return null|mixed
     *
     * Пытается получить данные с указанным именем из $_REQUEST, если они отсутствуют, то возвращает значение
     * по умолчанию (null или переданное пользовтелем)
     */
    protected function getData($name, $default_value = null)
    {
        if (isset($_REQUEST[$name])) {
            return $_REQUEST[$name];
        } else {
            return $default_value;
        }
    }

    /**
     * @return bool
     *
     * Является-ли массивом
     */
    private function isArray()
    {
        return is_array($this->field_value) === $this->rule_value;
    }

    /**
     * @return bool
     *
     * Больше чем переданный параметр
     */
    private function notLess()
    {
        return ($this->field_value >= $this->rule_value);
    }

    /**
     * @return bool
     *
     * Null или больше, ечм переданный параметр
     */
    private function nullOrNotLess()
    {
        return (is_null($this->field_value) || $this->notLess());
    }

    /**
     * @return bool
     *
     * Является-ли пустым
     */
    private function isEmpty()
    {
        return (empty($this->field_value) === $this->rule_value);
    }

    /**
     * @return bool
     *
     * Проверяет содержится-ли значение среди допустимых
     */
    private function available()
    {
        return (array_search($this->field_value, $this->rule_value) !== false);
    }

    /**
     * @return bool
     *
     * Проверяет является-ли значение допустимым почтовым адресом
     */
    private function email()
    {
        return ((bool)filter_var($this->field_value, FILTER_VALIDATE_EMAIL) === $this->rule_value);
    }

    /**
     * @return bool
     *
     * Проверяет является-ли значение строкой
     */
    private function string()
    {
        return (is_string($this->field_value) === $this->rule_value);
    }

    /**
     * @return bool
     *
     * Првоеряет является-ли значение логическим типом данных
     */
    private function boolean()
    {
        return (is_bool($this->field_value) === $this->rule_value);
    }

    /**
     * @return bool
     *
     * Проверсяет является-ли значение json-объектом
     */
    private function json()
    {
        json_decode($this->field_value);
        return ((json_last_error() == JSON_ERROR_NONE) === $this->rule_value);
    }

    /**
     * @return bool
     *
     * Значение является числом или null
     */
    private function integerOrNull()
    {
        return ((is_int($this->field_value) || is_null($this->field_value)) === $this->rule_value);
    }

    /**
     * @return bool
     *
     * Значение является числом
     */
    private function integer()
    {
        return (is_int($this->field_value) === $this->rule_value);
    }

    /**
     * @return bool
     *
     * Значение должно быть менее указанного
     */
    private function less()
    {
        return ($this->field_value < $this->rule_value);
    }

    /**
     * @return bool
     *
     * Значение должно быть больше указанного
     */
    private function greater()
    {
        return ($this->field_value > $this->rule_value);
    }

    /**
     * @return bool
     *
     * Должно быть строкой с длиной более указанной
     */
    private function stringLonger()
    {
        if (!is_string($this->field_value)) {
            return false;
        }
        return (mb_strlen($this->field_value) > $this->rule_value);
    }

    /**
     * @return bool
     *
     * Должно быть строкой с длиной менее указанной
     */
    private function stringShorter()
    {
        if (!is_string($this->field_value)) {
            return false;
        }
        return (mb_strlen($this->field_value) < $this->rule_value);
    }

    /**
     * @return bool
     *
     * Проверяет является-ли переданное значение корректным ip-адресом
     */
    private function ip()
    {
        return ((bool)filter_var($this->field_value, FILTER_VALIDATE_IP) === $this->rule_value);
    }

    private function regexp()
    {
        return (preg_match($this->rule_value, $this->field_value) === 1);
    }
}
