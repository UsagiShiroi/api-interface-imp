<?php

namespace Imp\Test;

use Imp\Model\Exception\InternalException;
use Imp\Model\Imp;
use Imp\Model\ImpReport;
use PHPUnit_Framework_TestCase;

class ImpReportTest extends PHPUnit_Framework_TestCase
{
    public function testAsArray()
    {
        $report = new ImpReport('some_data', Imp::SUCCESS_CODE);
        $report = $report->asArray();
        $this->assertInternalType('array', $report);
    }

    /**
     * @depends testAsArray
     */
    public function testSuccessReport()
    {
        $report = new ImpReport('some data', Imp::SUCCESS_CODE);
        $report = $report->asArray();
        $this->assertArrayHasKey('result', $report);
    }

    /**
     * @depends testAsArray
     */
    public function testErrorReport()
    {
        $report = new ImpReport('some data', (new InternalException())->getCode());
        $report = $report->asArray();
        $this->assertArrayHasKey('error', $report);
    }

    public function testAsJson()
    {
        $report = new ImpReport('some_data', Imp::SUCCESS_CODE);
        $report = $report->asJson();
        json_decode($report);
        $this->assertEquals(JSON_ERROR_NONE, json_last_error());
    }
}
