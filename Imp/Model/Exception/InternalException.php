<?php

namespace Imp\Model\Exception;

/**
 * Class InternalException
 * @package Imp\Model\Exception;
 * @api
 *
 * Ошибка для всех случаев, которые не были обработаны в других ошибках.
 */
class InternalException extends ImpException
{
    public function __construct()
    {
        parent::__construct('Произзошла внутренняя ошибка сервиса', 500);
    }
}
