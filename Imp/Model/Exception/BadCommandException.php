<?php

namespace Imp\Model\Exception;

/**
 * Class BadCommandException
 * @package Imp\Model\Exception;
 * @api
 *
 * Ошибка, сообщающая о том, что команда передана неверно.
 */
class BadCommandException extends ImpException
{
    public function __construct()
    {
        parent::__construct('Неверная команда', 400);
    }
}
