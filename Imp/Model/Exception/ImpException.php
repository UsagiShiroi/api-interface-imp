<?php

namespace Imp\Model\Exception;

use Exception;

/**
 * Class ImpException
 * @package Imp\Model\Exception;
 * @api
 * Все Exception'ы Imp'а должны быть унаследованы от этого класса
 */
class ImpException extends Exception
{
}
