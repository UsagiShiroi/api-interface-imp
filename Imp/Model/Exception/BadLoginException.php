<?php

namespace Imp\Model\Exception;

/**
 * Class BadLoginException
 * @package Imp\Model\Exception
 * @api
 *
 * Ошибка, сообщающая, что введенная пара логин-пароль не является допустимой для авторизации пользователя.
 */
class BadLoginException extends ImpException
{
    public function __construct()
    {
        parent::__construct('Указанные логин/пароль неверны', 330);
    }
}
