<?php

namespace Imp\Model\Exception;

/**
 * Class RequiredParameterMissingException
 * @package Imp\Model\Exception;
 * @api
 *
 * Ошиюка, обрабатывающая случай, когда не передан один или более обязательных параметров
 */
class RequiredParameterMissingException extends ImpException
{
    /**
     * @param array $params Массив с именами, отсутствующих параметров
     */
    public function __construct(array $params)
    {
        $message = 'Отсутствуют обязательные параметры: ';
        foreach ($params as $param) {
            $message .= $param . ', ';
        }
        $message = trim($message, ', ');
        parent::__construct($message, 300);
    }
}