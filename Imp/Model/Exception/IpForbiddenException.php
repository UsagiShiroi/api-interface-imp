<?php

namespace Imp\Model\Exception;

/**
 * Class IpForbiddenException
 * @package Imp\Model\Exception;
 * @api
 * Ошибка оповещающая о том, что указанный Ip отсутствует в списке адресов, которым разрешено пользование интерфейсом.
 */
class IpForbiddenException extends ImpException
{
    public function __construct()
    {
        parent::__construct('Доступ для данного Ip запрещен', 403);
    }
}
