<?php

namespace Imp\Model\Exception;

/**
 * Class ParameterValueException
 * @package Imp\Model\Exception;
 * @api
 *
 * Ошибка, отвечающая за обработку ошибок в параметрах
 */
class ParameterValueException extends ImpException
{
    /**
     * @param array $params Массив с именами ошибочных параметров
     */
    public function __construct(array $params)
    {
        $message = 'В значениях следующих параметров допущены ошибки: ';
        foreach ($params as $param) {
            $message .= $param . ', ';
        }
        $message = trim($message, ', ');
        parent::__construct($message, 301);
    }
}
