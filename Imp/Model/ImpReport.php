<?php

namespace Imp\Model;

/**
 * Class ImpReport
 * @package Imp\Model
 * @api
 *
 * Отчеты о работе Imp'a и их форматирование.
 */
class ImpReport
{
    /**
     * @var array $report Содержит в себе отчет по выполненному заданию
     */
    protected $report;

    /**
     * @param $data
     * @param $code
     *
     * Если код не соответствует успешному, то помещает данные в $error.
     * В ином случае помещает их в $result.
     * Это необходимо для дальнейшего формирования секций отчета
     */
    public function __construct($data, $code)
    {
        $report = [
            'time' => date('Y-m-d H:i:s', time()),
            'code' => $code,
        ];
        if ($code !== Imp::SUCCESS_CODE) {
            $report['error'] = $data;
        } else {
            $report['result'] = $data;
        }
        $this->report = $report;
    }

    /**
     * @return string Отчет по заданию в Json-формате
     */
    public function asJson()
    {
        $json_report = json_encode($this->report);
        return $json_report;
    }

    /**
     * @return array Возвращает отчет в виде массива
     */
    public function asArray()
    {
        return $this->report;
    }
}
