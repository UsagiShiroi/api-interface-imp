<?php

namespace Imp\Model\Task;

class Example
{
    /**
     * Подключаем, чтобы иметь возможность использовать валидацию
     */
    use ImpValidateTrait;
    /**
     * @var array В данном массиве описываются правила валидации, которые необходимо применить к тем или иным
     * данным в следующем формате:
     *
     * {имя параметра в $_REQUEST} => [{название правила} => {значение}]
     *
     * Для работы валидации класс должен использовать trait, унаследованный от ImpValidateTrait
     * Правило должно быть реализовано, как метод с указанным именем, который должен возвращать boolean
     */
    protected $rules = [
        'names' => ['isArray' => true],
        'message' => ['stringLonger' => 0, 'stringShorter' => 20],
    ];

    public function validationExample()
    {
        /* Определяем список обязательных полей */
        $this->required(['message']);

        /* Получаем данные любым удобным способом */
        $names = $this->getData('names', ['Vasya', 'Masha']);
        $message = $this->getData('message');
        /* Валидируем значения */
        $this->validate(['names' => $names, 'message' => $message]);

        return 'Example succeed';
    }
}
