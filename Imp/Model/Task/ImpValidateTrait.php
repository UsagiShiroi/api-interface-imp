<?php

namespace Imp\Model\Task;

use Imp\Model\Exception\ParameterValueException;
use Imp\Model\Exception\RequiredParameterMissingException;
use Middleware\ValidateTrait;

/**
 * Class ImpValidateTrait
 * @trait ImpValidateTrait
 * @package Imp\Model\Task
 */
trait ImpValidateTrait
{
    use ValidateTrait {
        validate as parentValidate;
        required as parentRequired;
    }

    /**
     * @param $fields
     * @throws ParameterValueException
     * Производит валидацию значений переданных полей
     */
    protected function validate($fields)
    {
        $validation_errors = $this->parentValidate($fields);
        if (!empty($validation_errors)) {
            throw new ParameterValueException($validation_errors);
        }
    }

    /**
     * @param $fields
     * @throws RequiredParameterMissingException
     * Проверяет переданы-ли все необходимые параметры
     */
    protected function required($fields)
    {
        $missing_fields = $this->parentRequired($fields);
        if (!empty($missing_fields)) {
            throw new RequiredParameterMissingException($missing_fields);
        }
    }
}
