<?php

namespace Imp\Model;

use Imp\Model\Exception\BadCommandException;
use Imp\Model\Exception\ImpException;
use Imp\Model\Exception\InternalException;
use Imp\Model\Exception\IpForbiddenException;
use Exception;

/**
 * Class Imp
 * @package Imp\Model
 * @api
 * Класс осуществляющий обработку запросов
 */
class Imp
{
    /**
     * Код успешной операции - говорит об отсутствии ошибок
     */
    const SUCCESS_CODE = 200;
    /**
     * @var array $allowed_ips Список Ip-адресов имеющих доступ к вызову класса
     */
    private $allowed_ips = [
        '127.0.0.1',
    ];
    /**
     * @var string Определяет формат ответа сервиса,по умолчанию JSON
     */
    private $response_type = 'json';
    /**
     * @var ImpReport Отчет о работе
     */
    private $report;

    /**
     * @throws IpForbiddenException Запрос пришел с IP, которого нет в списке допустимых
     *
     * Осуществляет проверку вызывающего на возможность пользования интерфейсом
     */
    public function auth()
    {
        if (isset($_REQUEST['response_type'])) {
            $this->response_type = $_REQUEST['response_type'];
        }
        if (!in_array($_SERVER['REMOTE_ADDR'], $this->allowed_ips)) {
            throw new IpForbiddenException();
        }
    }

    /**
     * @return ImpReport
     *
     *  Проверяет команду на правильность ввода, после чего исполняет ее и формирует отчет
     */
    public function work()
    {
        try {
            $this->auth();
            $command = $this->getCommand();
            $task = $this->getTask($command[0]);
            $method_name = $command[1];
            $this->checkMethod($task, $method_name);
            $result = $this->execute($task, $method_name);
            $this->report($result);
        } catch (Exception $exception) {
            if ($exception instanceof ImpException) {
                $this->error($exception);
            } else {
                $this->error(new InternalException());
            }
        }
        return $this->report;
    }

    /**
     * @param ImpException $exception Произошедшая ошибка
     * @return ImpReport
     *
     * Получает на вход ошибку и формирует по ней отчет.
     */
    protected function error(ImpException $exception)
    {
        return $this->report([$exception->getMessage()], $exception->getCode());
    }

    /**
     * @param mixed $data Содержимое для вывода в полях result или error
     * @param int $code Код операции
     * @return ImpReport Отчет о произошедшем
     */
    protected function report($data, $code = self::SUCCESS_CODE)
    {
        $this->report = new ImpReport($data, $code);
        return $this->report;
    }

    /**
     * Производит ответ на запрос, согласно формату ответа, формат не соответсвует ни одному из имеющихся,то
     * отыечает в формате JSON
     */
    public function respond()
    {
        switch ($this->response_type) {
            case 'json':
                $this->returnJson($this->report);
                break;
            default:
                $this->returnJson($this->report);
                break;
        }
    }

    /**
     * @param ImpReport $report Отчет об операции
     *
     * Осуществляет вывод содержимого отчета в формате Json
     */
    protected function returnJson(ImpReport $report)
    {
        $report = $report->asJson();
        header('Content-Type: application/json');
        print($report);
    }

    /**
     * @throws BadCommandException Ошибка, говорящая, что запрос сформирован неверно
     * @return array Массив, содержащий тип задачи и запрашиваемый метод
     *
     * Проверяет команду на правильность написания и возфращает массив из двух частей
     * 0 - тип выполняемой задачи
     * 1 - запрашиваемый метод
     */
    protected function getCommand()
    {
        $command = trim($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']);
        $command = trim($command, '?');
        $command = trim(trim($command, '/imp/'), '/');
        $command = explode('/', $command);
        if (count($command) !== 2) {
            throw new BadCommandException();
        }
        return $command;
    }

    /**
     * @throws BadCommandException Ошибка, говорящая, что запрос сформирован неверно
     * @param string $task_type Тип задания
     * @return mixed Обрабочтик задания
     *
     * Проверяет существует-ли обработчик для данного типа заданий и возвращает его в случае успеха.
     */
    protected function getTask($task_type)
    {
        $class_name = __NAMESPACE__ . '\\Task\\' . ucfirst($task_type);
        if (!class_exists($class_name)) {
            throw new BadCommandException();
        }
        $task = new $class_name();
        return $task;
    }

    /**
     * @throws BadCommandException Ошибка, говорящая, что запрос сформирован неверно
     * @param mixed $task Обработчик
     * @param string $method_name Запрашиваемый метод
     *
     * Проверяет существует-ли в обработчике запрашиваемый метод
     */
    protected function checkMethod($task, $method_name)
    {
        if (!method_exists($task, $method_name)) {
            throw new BadCommandException();
        }
    }

    /**
     * @param mixed $task Обрабочтик задания
     * @param string $method Запрашиваемое действие
     * @return array Результат выполнения запрошенного действия
     *
     * Исполянет действия и возращает результат выполнения
     */
    protected function execute($task, $method)
    {
        $result = $task->{$method}();
        return $result;
    }
}
