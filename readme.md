Модуль интерфейса взаимодействия посредством http - запросов.
На данный момент принимает запросы только с Ip-адрессов, указанных в основном классе.
Запрос должен иметь вид {api_url}/{group}/{action}
Где group - имя класса в папке Task, а action - имя метода этого класса.
Запрос может быть как методом GET, так и методом POST. Параметры получаем из $_REQUEST

Imp\Model\Imp - основной класс, вся работа производится через него. Обрабатывает входящий запрос, после
чего вызывает запрашиваемый метод из нужного класса. После чего формирует отчет о работе и отдает его в указанном
формате

Imp\Model\ImpReport - отчет о работе интерфейса.

Exception - содержит классы ошибок

Imp\Model\Exception\ImpException - Все ошибки должны наследоваться от этой.
Imp\Model\Exception\RequiredParameterMissingException - код ошибки 300, обязательный параметр отсутствует
Imp\Model\Exception\ParameterValueException - код ошибки 301, ошибка в значении параметра.
Imp\Model\Exception\BadLoginException - код ошибки 330, данные для авторизации пользователя неверны
Imp\Model\Exception\BadCommandException - код ошибки 400, неверная команда
Imp\Model\Exception\IpForbiddenException - код ошибки 403, доступ для данного Ip запрещен
Imp\Model\Exception\InternalException - код ошибки 500, внутреняя ошибка панели, не являющаяся ImpException

Task - содержит методы для работы с панелью, разделенный по группам

Imp\Model\Task\Example - пример
Imp\Model\Task\ImpValidateTrait - при ошибке валидации кидает исключение нужного типа.